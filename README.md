# Práctica nuevo ingreso
## Software Development (Front-end)
#### Crear sistema de tienda online, esta solo constara de dos partes:
	1.	Sección para ver los productos a la venta
	2.	Carrito de compras
#### La práctica debe de cumplir con los siguientes requerimientos:
	1.	Creado con VueJS
	2.	Ser totalmente responsive
	3.	Estar bien estructurado
#### Herramientas a utilizar
	1.	nodeJS: https://nodejs.org/es/download/
	2.	VueJS documentation: https://vuejs.org/guide/introduction.html
	3.	Visual Studio Code: https://code.visualstudio.com/download
	4.	Github: https://github.com/